#include "../include/Datu_egiturak.h"
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include "../include/config.h"

struct Thread *lagun;

//SJF Scheduler
void exekutatuSJF()
{

    for (int i = 0; i < pc.num_cpu; i++)
    {
        for (int j = 0; j < pc.cpu_lista->num_core; j++)
        {
            for (int k = 0; k < pc.cpu_lista->core_lista->num_thread; k++)
            {
                lagun = &pc.cpu_lista[i].core_lista[j].thread_lista[k];

                if (lagun->free == 1)

                {

                    lagun->pcb.duration--;
                    if (lagun->pcb.duration == 0)
                    {
                        lagun->free = 0;
                    }
                }

                if (lagun->free == 0 && pcbKantitatea > 0)
                {
                    lagun->free = 1;
                    lagun->pcb = ilara[pcbKantitatea - 1];

                    pcbKantitatea--;
                }

                printf("CPU: %d, Core: %d, Thread: %d, Free: %d, Duration: %d, PID: %d\n",
                       i, j, k, lagun->free, lagun->pcb.duration, lagun->pcb.pid);
            }
        }
    }

    printf("\n\n");
}

//Round Robin Scheduler
void exekutatuRR()
{

    for (int i = 0; i < pc.num_cpu; i++)
    {
        for (int j = 0; j < pc.cpu_lista->num_core; j++)
        {
            for (int k = 0; k < pc.cpu_lista->core_lista->num_thread; k++)
            {
                lagun = &pc.cpu_lista[i].core_lista[j].thread_lista[k];

                if (lagun->free == 1)

                {

                    lagun->pcb.duration--;
                    lagun->pcb.quantum--;
                    if (lagun->pcb.duration == 0)
                    {
                        lagun->free = 0;
                    }
                    else if (lagun->pcb.quantum == 0)
                    {
                        lagun->free = 0;
                        lagun->pcb.quantum = 4;
                        ilara[pcbKantitatea] = lagun->pcb;
                        pcbKantitatea++;
                    }
                }
                else
                {

                    if (lagun->free == 0 && pcbKantitatea > 0)
                    {
                        lagun->free = 1;
                        lagun->pcb = ilara[0];
                        for (int l = 0; l < pcbKantitatea; l++)
                        {
                            ilara[l] = ilara[l + 1];
                        }
                        pcbKantitatea--;
                    }
                }
                printf("CPU: %d, Core: %d, Thread: %d, Free: %d, Duration: %d, PID: %d\n",
                       i, j, k, lagun->free, lagun->pcb.duration, lagun->pcb.pid);
            }
        }
    }

    printf("\n\n");
}

//SJF eta RR aukeratzeko funtzioa
void *scheduler(void *arg)
{

    if (policy == 0)
        exekutatuSJF();
    else
    {
        exekutatuRR();
    }

    return NULL;
}