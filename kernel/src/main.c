#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../include/config.h"
#include "../include/Datu_egiturak.h"
#include "../include/process_create.h"
#include "../include/Scheduler.h"


int done;
int prest;
int tenp_kop;
int sortu;
int timer_proz;
int timer_scheduler;
int pcbKantitatea;
int policy;
int frek_proz;
int frek_scheduler;
struct PCB ilara[100];
pthread_cond_t cond;
pthread_cond_t cond2;
pthread_mutex_t lock;


struct CPU *cpu_lista;
struct PC pc;
int cpu_kop;
int core_kop;
int thread_kop;

void initialize()
{
  cpu_lista = (struct CPU *)malloc(sizeof(struct CPU) * cpu_kop);
  for (int i = 0; i < cpu_kop; i++)
  {
    cpu_lista[i].core_lista = (struct Core *)malloc(sizeof(struct Core) * core_kop);
    for (int j = 0; j < core_kop; j++)
    {
      cpu_lista[i].core_lista[j].thread_lista = (struct Thread *)malloc(sizeof(struct Thread) * thread_kop);
      for (int k = 0; k < thread_kop; k++)
      {
        cpu_lista[i].core_lista[j].thread_lista[k].free = 0;
        cpu_lista[i].core_lista[j].thread_lista[k].pcb.pid = -1;
      }
    }
  }
}

int main(int argc, char *argv[])
{

  printf("Sartu CPU kopurua: ");
  scanf("%d", &cpu_kop);
  printf("Sartu Core kopurua: ");
  scanf("%d", &core_kop);
  printf("Sartu Thread kopurua: ");
  scanf("%d", &thread_kop);
  printf("Sartu politika, 0 SJF eta 1 ROUND ROBIN: ");
  scanf("%d", &policy);
  printf("Sartu Process Creator-aren frekuentzia: ");
  scanf("%d", &frek_proz);
  printf("Sartu Scheduler-aren frekuentzia: ");
  scanf("%d", &frek_scheduler);

  pthread_mutex_init(&lock, NULL);

  pthread_t tid;
  pthread_t tid2;
  pthread_t tid4;

  done = 0;
  prest = 0;
  tenp_kop = 2;
  sortu = 1;
  timer_proz = 0;
  timer_scheduler = 0;

  initialize();
  pc.cpu_lista = cpu_lista;
  pc.cpu_lista->core_lista = cpu_lista->core_lista;
  pc.cpu_lista->core_lista->thread_lista = cpu_lista->core_lista->thread_lista;
  pc.num_cpu = cpu_kop;
  pc.cpu_lista->num_core = core_kop;
  pc.cpu_lista->core_lista->num_thread = thread_kop;

  pthread_create(&tid2, NULL, clock_SE, NULL);

  pthread_create(&tid, NULL, timer_Process, NULL);
  pthread_create(&tid4, NULL, timer_Scheduler, NULL);
  printf("Sartu 0 amaitzeko: ");
  while (sortu)
  {

    scanf("%d", &sortu);
  }

  pthread_join(tid, NULL);

  pthread_mutex_destroy(&lock);

  for (int i = 0; i < cpu_kop; i++)
  {
    for (int j = 0; j < core_kop; j++)
    {
      free(cpu_lista[i].core_lista[j].thread_lista);
    }
    free(cpu_lista[i].core_lista);
  }
  free(cpu_lista);
  printf("Programa amaitu da\n");
  return 0;
}
