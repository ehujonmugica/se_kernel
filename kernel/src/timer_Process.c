#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "../include/config.h"
#include "../include/process_create.h"

void *timer_Process(void *arg)
{

    int kont = 0;

    //Erlojua prest dagoen arte itxaron
    while (prest == 0)
    {
    }
    printf("Timera hasieratua\n");

    //Mutex-a blokeatu
    pthread_mutex_lock(&lock);

    while (sortu)
    {

        done++;
        kont++;
        if (kont >= frek_proz)
        {
            //Prozesua sortu
            prozesua_sortu((void *)1);
            kont = 0;
        }

        //Seinalea bidali
        pthread_cond_signal(&cond);
        //Mutexa askatu eta seinaleari itxaron
        pthread_cond_wait(&cond2, &lock);
    }
    return NULL;
}