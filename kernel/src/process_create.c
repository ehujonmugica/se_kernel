#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../include/Datu_egiturak.h"
#include "../include/process_create.h"
#include "../include/config.h"

int pid = 1;
//Prozesu bat sortzeko funtzioa
struct PCB pcb_Creator()
{

     struct PCB pcb;

     pcb.pid = pid; //Prozesuaren identifikadorea
     pid++; 
     pcb.state = 0; //Prozesuaren egoera
     pcb.priority = 0; //Prozesuaren lehentasuna
     pcb.quantum = 4; //Prozesuaren quantum
     pcb.duration = (rand() % 20) + 1; //Prozesuaren exekuzio iraupena

     return pcb;
}

// SJF Scheduler
int pos = 0;
void add_to_queueSJF()
{

     if (pcbKantitatea > 99)
     {
     }
     else
     {

          struct PCB lagun = pcb_Creator();

          if (pcbKantitatea == 0)
          {

               ilara[0] = lagun;
          }
          else
          {
               pos = pcbKantitatea - 1;
               while (pos >= 0 && ilara[pos].duration < lagun.duration)
               {
                    ilara[pos + 1] = ilara[pos];
                    pos--;
               }

               ilara[pos + 1] = lagun;
          }
          pcbKantitatea++;
     }
}

// Round Robin Scheduler
void add_to_queueRR()
{

     if (pcbKantitatea > 99)
     {
     }
     else
     {

          struct PCB lagun = pcb_Creator();

          if (pcbKantitatea == 0)
          {

               ilara[0] = lagun;
          }
          else
          {
               ilara[pcbKantitatea] = lagun;
          }
          pcbKantitatea++;
     }
}

//SJF eta RR politikak erabiltzen dituen prozesuak sortzeko funtzioa
void *prozesua_sortu(void *arg)
{

     if (policy == 0)
          add_to_queueSJF();
     else if (policy == 1)
     {
          add_to_queueRR();
     }

     return NULL;
}
