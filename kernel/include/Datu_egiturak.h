#include <stdint.h>
extern int pcbKantitatea;

struct mm
{
  uint32_t data;
  uint32_t code;
  uint32_t pgb;
};

struct PCB
{

  int pid;
  int state;
  int priority;
  int quantum;
  int duration;
  struct mm mm;
};

struct TLB
{

  uint32_t virtual_address;
  uint32_t physical_address;
  uint32_t status_bits;
};

extern struct PCB ilara[100];

struct Thread
{
  struct PCB pcb;
  int free;
  uint32_t PC;
  uint32_t IR;
  uint32_t PTBR;
  struct TLB tlb;
};

struct Core
{
  struct Thread *thread_lista;
  int num_thread;
};

struct CPU
{
  struct Core *core_lista;
  int num_core;
  
};

struct PC
{
  struct CPU *cpu_lista;
  int num_cpu;
};

extern struct PC pc;
