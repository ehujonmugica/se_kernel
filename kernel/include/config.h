#include <stdio.h>
#include <stdlib.h>

extern int done;
extern int prest;
extern int tenp_kop;
extern pthread_cond_t cond;
extern pthread_cond_t cond2;
extern pthread_mutex_t lock;
extern int timer_proz;
extern int sortu;
extern int timer_scheduler;
extern int policy;
extern int frek_proz;
extern int frek_scheduler;

void *clock_SE(void *arg);
void *timer_Process(void *arg);
void *timer_Scheduler(void *arg);
